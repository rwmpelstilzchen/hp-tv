%! TEX program = lualatex

\documentclass[a4paper, 12pt, oneside]{memoir}
\OnehalfSpacing
\pagestyle{simple}
\usepackage{microtype}

\usepackage{fontspec}
\setmainfont{Vesper Pro}
\setmonofont[Scale=MatchLowercase]{Iosevka}
\newcommand{\C}[1]{\emph{#1}}
\newcommand{\E}[1]{\emph{#1}}
\newcommand{\foreign}[1]{\emph{#1}}
\newcommand{\gram}[1]{\textsc{#1}}
\newcommand{\optional}[1]{{\small [#1]}}

\newcommand{\click}{{\fontspec{Symbola}⏩}}
\newcommand{\anticlick}{{\fontspec{Symbola}⏪}}

\usepackage[super]{nth}

\begin{document}

Hi,

%\optional{First of all, know your audience: is there anyone here who hasn’t read at least the first Harry Potter book or watched the movie (it’s OK to raise your hand…).

%OK. Good. Now is there anyone here who doesn’t speak fluent Welsh?}

{\click} Today I will present some of my findings concerning the way Emily Huws made use of the Welsh second person system when translating \textit{Harry Potter and the Philosopher’s Stone}. Welsh and English are quite different in this respect and she had to make distinctions where the original does not make them. Our concern here is with those of sociopragmatic nature.



\section*{Introduction}

{\click} Now Welsh, for anyone who is not familiar with this language, is a Celtic language spoken by about half a million people in Wales. {\click} Today we’ll discuss a very {\click} ‘European’ areal phenomenon, the T-V distinction, but generally Welsh is quite exceptional in the Standard Average European landscape, as are other Celtic languages.

{\click} So the first Harry Potter book was published in 1997 and was since translated into more than seventy languages. One of them is Welsh, into which it was translated by a children’s author speaking my favourite dialect, Arfonian.



\section*{\C{ti:chi}}

{\click} We all know the T-V distinction of politeness or honorificity, in which usually the \gram{2sg} T-form serves a ‘familiar’ form and the \gram{2pl}, V, as either a true plural or a ‘formal’ singular. These terms, ‘T’ and ‘V’, were coined by Brown and Gilman’s in their seminal article, on the basis of Latin \foreign{tū} and \foreign{vōs}. {\click} T-V distinctions are common throughout Europe, in Indo-European and non-Indo-European languages alike. In Welsh the forms are \C{ti} and \C{chi}, {\click} which come from Proto-Indo-European corresponding pronouns, *\textit{túh₂} and *\textit{wos}.

{\click} Since English \E{you} lacks politeness and number distinctions, the translator was obliged to make those distinctions according to her understanding of the text, making the output as natural as possible in the target language. The number distinction is relatively trivial (and even expressed in the English reflexive \E{yourself:yourselves}), so we will not deal with it at all. The politeness distinction, on the other hand, is very interesting. Therefore, we’ll discuss only \gram{2sg}, in which we do have politeness distinction, expressed through \C{ti}:\C{chi} structural opposition.

All these nice colours {\click} make me think about role of the translator here as a prism, {\anticlick} with \E{you} as a white light and \C{ti} and \C{chi} as spectral colours.

{\click} The loci of the T-V distinction are in independent as well as dependent pronouns, including the verbal system. One locus which does not correspond to English \E{you} is the imperatives, which are generally marked by a zero pronoun in English.

{\click} I’ve manually checked and tagged \emph{all} 1393 occurrences of \emph{you}, \emph{your}, etc.\ according to {\click} ‘speaker’, ‘addressee’ and \C{‘ti:chi’} information (who speaks to whom how). From these tags {\click} I’ve automatically derived an intricate map representing sociopragmatic relationships expressed through the \C{ti:chi} distinction. What I present here are some aspects of the system that emerges from this map by finding generalizations and common classes.

{\click} Let’s begin. We’ll discuss three topics:
\begin{itemize}
	\item change in address form,
	\item addressing someone unknown (such as someone knocking on the door),
	\item and the relationship between children and grown-ups.
\end{itemize}



\subsection*{Change in address form}

The relationships between characters are not set in stone; there are some cases in which transitioning from one address form to the other one signals change in relationship.

{\click} For example, when Harry first meets Hagrid he is a stranger, a grown-up man whom Harry doesn’t know, so he speaks to him using the formal \C{chi}, {\click} but as Hagrid tells his story and the close connection between them is revealed, Harry transitions to using \C{ti}, which he continues to do throughout the book. Hagrid is a very interesting character sociopragmatically, standing somewhere between the teachers and the students.

{\click} Thus it is not surprising that as the special mixed-age clique of Harry, Hermione, Ron and Hagrid develops over time {\click} Hermione starts using \C{ti} towards Hagrid as well. \optional{In this particular example the use of \C{ti} might also act as a rhetorical device of flattering, signalling closeness.}

{\click} With Vernon, Harry’s adoptive father, the transition when speaking to Hagrid bears a completely different meaning: from politeness (even when demanding Hagrid to leave) {\click} to direct, straightforward harsh speech the moment Hagrid is going to tell Harry the secret Vernon fears the most: that Harry is a wizard and his parents, who were wizards as well, were killed magically.

{\click} In this example Firenze the centaur speaks to what was for him at the moment a random Hogwarts student with \C{ti}, but when he realizes it is actually Harry he transitions to \C{chi}, showing him respect. This is not a development in relationship, but a change caused by not recognizing the addressee correctly at first.



\subsection*{Unknown~/ non-specific addressee}

What happens when you don’t know who is your addressee or how many they are? The answer Emily Huws gives us for the Welsh language is to be on the safe side: use \C{chi}, not \C{ti} which is too loaded with pragmatic information, too specific to be used in such a situation.

{\click} \E{Who’s there?} \C{Pwy sy ’na?}.
\begin{itemize}
	\item {\click} \C{chi} when someone knocks on the door (or rather ‘knocks the door’ in this case…);
	\item {\click} \C{chi} when something is moving in the Forbidden Forest (only to be revealed as Ronan afterwards, whom Hagrid greets with \C{ti});
	\item {\click} \C{chi} when Harry, Hermione and Ron are hidden beneath the Invisibility Cloak.
\end{itemize}

{\click} When you don’t know who will read what you write you use \C{chi}: {\click} be it book titles {\click} or a written puzzle.

{\click}{\click} When Rowling uses the literary technique of addressing the readers (with impersonal \nth{2} person), it is translated with \C{chi} as well, {\click} as is generic address within Free Indirect Speech. This last example is interesting, because if we turn the pages back to when ‘Gringotts is the safest place etc.’ was actually said, {\click} it was Hagrid speaking familiarly with Harry. {\anticlick} So the use of \C{chi} here does not mirror the original wording but the norm of this literary technique.



\subsection*{Age and status}

\subsubsection*{Students and teachers}

{\click} Next to our final section. {\click} Let’s see, students address teachers with \C{chi} and each other with \C{ti}. Fair enough, that’s reasonable. McGonagall and Dumbledore are more or less on the same level, being Deputy Headmistress and Headmaster respectively. Snape and Quirrell are both teachers. And Quirrell addresses Dumbledore, his superior, using \C{chi}. All make sense.

But what might seem, \foreign{a priori}, surprising is why teachers use \C{chi} toward students, who rank lower in the school hierarchy. The answer is that this is how things are in Welsh society, or at least how they used to be. {\click} Peter Wynn Thomas, who wrote this monumental~— and lovely~— piece of scholarship says that at least in the ’60s students and teachers used \C{chi} reciprocally, and this was changed later (when? He doesn’t say) so that an irreciprocal relationship is now more usual. With parent\textasciitilde child relationships there was an opposite development; now the tendency is to use \C{ti} reciprocally within the family. \textit{(You have the relevant paragraph on your handouts, just for you to see how Welsh linguistic text looks like…).} Why do Hogwarts teachers address students in the older way? I can think of two possible answers: {\click} one is that this is what the translator is used to from \emph{her} days at school, and the other is that Hogwarts, being an old-fashioned school, is linguistically presented as such. Non-teacher staff, by the way, usually use \C{ti} towards students. {\click} A short digression: here is an extreme example from the corpus of my dissertation, which supports Thomas’ statement, es expected: in this situation, taken place at the beginning of last century, a headmaster addressed a student using \C{chi} just before caning her… So being kind and using \C{chi} are two different things, on two different planes.

{\click} This helps us understand why Quirrell the Defence Against the Dark Arts teacher, who had used \C{chi} towards Harry from the moment he met him, {\click} continues using \C{chi} even after being revealed as a villain: one can be evil and still use \C{chi}, just like Kate Roberts’ Headmaster in the previous example. In the same situation {\click} Voldemort, the antagonist, addresses Harry with \C{ti}, but he is neither Harry’s teacher nor wish to show him any respect.

{\click} If we have a second look at the table, we will see there \emph{are} occasions in which Harry and Neville are addressed with \C{ti} by teachers.

{\click} Let’s begin with Neville, who is addressed with \C{ti} by both Madam Hooch the Flying teacher and Professor Snape the Potions teacher. If we have a closer look, we will see that both cases this happens just after he acted… well, quite clumsily… which resulted in {\click} a broken wrist in one case, {\click} or a mayhem in class in the other. So we can understand these teachers not addressing him with \C{chi} in these particular situations.

{\click} With Dumbledore and Harry, I’m not as sure. {\click} Dumbledore the Headmaster addresses Harry the same way he does with Hagrid and McGonagall. It is a systematic choice by the translator, with more than sixty occurrences. If we look at the situations in which Dumbledore talks to Harry, we will see that in all three situations they speak alone, or quietly enough so that only Harry can hear. These are not class or a class-like situations. My hypothesis is that the use of \C{ti} by Dumbledore signals special closeness, not considering Harry as ‘just another Hogwarts students’ to be addressed by the distancing \C{chi}. It’s a pity the other books were not translated, so we cannot check this hypothesis…



\subsubsection*{Children and their (adoptive) parents}

{\click} I remind you that it is now normal to use \C{ti} reciprocally within the family.

{\click} The relationship between the Dursleys and their adopted child Harry is not a loving one. That’s reflected by the {\click} irreciprocal use of \nth{2} person, {\click} as opposed to the reciprocal one with their biological son Dudley. {\click} The Weasleys, on the other hand, are the opposite, using the familial \C{ti} reciprocally. So the Weasleys are linguistically signalled as a family in contemporary Welsh, while the Dursley-Potter hierarchical irreciprocal use of the \nth{2} person is characteristic of their abusive relationship.



\subsubsection*{Grown-ups \C{chi}-ing Harry}

Understandably, in a story where the ‘ugly duckling’ Harry grows up to be a wizard, ‘a beautiful swan’, the respect people show Harry, who defeated the dreaded Voldemort, is expressed by linguistic means. (We’ve seen this with Firenze the centaur earlier). {\click} This is most pronounced in the Leaky Cauldron pub, where strangers are delighted to see Harry, shaking hands and all. This is the main literary purpose of this scene, showing Harry respect.

{\click} Later on Harry and Hagrid’s visit to Diagon Alley they meet Ollivander the wand-maker, who speaks to Harry in \C{chi}, {\click} but to Hagrid in \C{ti}, not forgetting to remind him he was expelled from Hogwarts… As you can see in the {\click} next example, when another seller does not recognize Harry, she uses \C{ti}.



\section*{Conclusion}

%{\click} I will finish with a nice example, not related to anything else I said. At the end of the book it is revealed that the villain is, surprisingly, Professor Quirrell, who has Voldemort’s face attached to the back of his head. {\click} Two faces on one head speak, one is master, one is servant. You can guess who speaks to whom how…

So, I want you to take two things from this talk.

{\click} The first is the complex and interesting Welsh sociopragmatic system, as reflected by the obligatory translation choices arising from the different systematics of the original and target languages. It might not be identical to original fiction or spontaneous speech, but it can stand as a valid sub-kind of language with its own systematics.

%\E{you}{\fontspec{DejaVu Serif}⇢}T-V is one case study of how a translator deals with the obligatory translation choices arising from the different systematics of the original and target languages. This case offers us a glimpse into Welsh sociopragmatics, as reflected in a magical world.

{\click} The second thing I want you to take is the potential for typological comparison using translations of a single text. The research I’ve presented here is a part of a comparative cross-linguistic project exploring the sociopragmatics of second person using the first Harry Potter book. This has some similarities to recent works done on film translations. The project is in an embryonic stage, and I hope it will be successful. It makes use of this one book which has diverse interpersonal relations and was translated into dozens languages, many of which have a T-V distinction. What are the relevant differences between translations in the distribution of the pronouns? How do they reflect the target languages and the social norms of their respective speech communities? {\click} This is for us to discover; so if you work on a language with a T-V distinction \textit{Harry Potter} was translated into, please join us!

{\click} \C{Diolch yn fawr iawn… i chi!}


\section*{Appendix I}

\optional{{\click} A quick remark about the impersonal use use of \nth{2} person. When translating impersonal, or ‘generic’, \E{you}s the translator usually used the Welsh \nth{2} person system, encoding and reflecting the sociopragmatic relation between the speaker and the addressee just like in actual, referential usage: if I would address you with the familiar \C{ti} I will do so in generic \nth{2} as well, and accordingly with the formal \C{chi}. The other, less commonly used strategy is to use non-personal constructions, as you can see in the {\click} first {\click} two examples. In addition, {\click} some \emph{you}s in more-or-less bound phrases like \E{tellyou} or \E{thankyou} can be zeroed. This is all for making the text idiomatic in Welsh.}
\end{document}
