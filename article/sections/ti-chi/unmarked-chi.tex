\subsection{Hidden or non-specific addressees}
\label{sec:unmarked-chi}

The \C{ti:chi} opposition is grounded in the concrete pragmatic situation.
This raises a question regarding circumstances in which this situation is unclear.
When one does not know with whom they communicate, \C{chi} is used exclusively in the corpus, \C{ti} being too loaded, too specific and direct for such a situation.
This can be viewed as a ‘safe’%
\footnote{The use of an indirect way of address (here, \C{chi}) is a common strategy for avoiding possibly face-threatening acts \parencite{goffman.e:1967:face-work} towards the addressee; see \textcite{brown.p+:1987:politeness} and \textcite{malsch.d.l:1987:number}.}
or ‘pragmatically unmarked’ option, both with regards to number (not knowing how many people one is addressing) and to politeness (not knowing the nature of the interpersonal relationship with the addressee(s)).

Two types of cases are examined: addressing someone hidden and addressing a non-specific reader.



\subsubsection{Hidden addressees}

These three examples, \crefrange{ex:hidden door}{ex:hidden cloak}, begin with the question ‘Who’s there?’ \C{(Pwy sy ’na?)}, addressing someone unseen:

In \cref{ex:hidden door} someone is outside knocking on the door. Vernon Dursly, who is inside, does not know who is out there.

\example{4}{39}{36}{}{hidden door}
{
%	\bi
%	{There was a crash behind them and Uncle Vernon came skidding into the room. He was holding a rifle in his hands~— now they knew what had been in the long, thin package he had brought with them.}
%	{Clywyd trwst mawr tu cefn iddyn nhw a sglefriodd Yncl Vernon i mewn i’r ystafell. Cydiai mewn reiffl~— a sylweddolodd pawb beth fu yn y parsel hir, cul roedd o wedi dod gyda nhw.}
%
	\bi
	{‘Who’s there?’ he shouted. ‘I warn \hlyou{you}~— I’m armed!’}
	{‘Pwy sy ’na?’ gwaeddodd. ‘Dwi’n eich rhybuddio \hlchiunknown{chi}~— mae gen i wn!’}
}

\noindent
In \cref{ex:hidden forest} Hagrid senses something moving in the thick Forbidden Forest. At first, he uses \C{chi}, but after it is revealed it was Ronan, with whom Hagrid is on familiar terms, he greets him with \C{ti}%
\footnote{Note that the \C{chdi} pronoun used in this example and in \cref{ex:Fred=Molly} is a colloquial Northern T-form, unrelated to \C{chi} even though it begins with a \C{ch-} \IPA{/χ-/} sound. See \textcite[§~4.3]{willis.d:2017:chdi} for an etymological discussion.}.

\example{15}{184}{200}{}{hidden forest}
{
%	\bi
%	{They walked more slowly, ears straining for the faintest sound. Suddenly, in a clearing ahead, something definitely moved.}
%	{Ymlaen â nhw’n arafach, gan glustfeinio am y smic lleiaf o sŵn. Yn sydyn, mewn llannerch o’u blaenau, symudodd rhywbeth.}
%
	\bi
	{‘Who’s there?’ Hagrid called. ‘Show \hlyou{yerself}~— I’m armed!’}
	{‘Pwy sy ’na?’ galwodd Hagrid. ‘\hlchiunknown{Dowch} i’r golwg; ma’ gin i arfa!’}

	%\bi
	%{And into the clearing came~— was it a man, or a horse? To the waist, a man, with red hair and beard, but below that was a horse’s gleaming chestnut body with a long, reddish tail. Harry and Hermione’s jaws dropped.}
	%{Ac i’r llannerch daeth~— ai dyn oedd o, neu geffyl? Uwchben y gwasg, dyn barfog, gwalltgoch ydoedd, ond islaw roedd corff ceffyl lliw castan disglair, ac iddo gynffon hir, gochlyd. Syrthiodd cegau Harri a Hermione ar agor.}

	\bi
	{[…]}
	{[…]}

	\bi
	{‘Oh, it’s \hlyou{you}, Ronan,’ said Hagrid in relief. ‘How are \hlyou{yeh}?’}
	{‘O, \hlchdi{chdi} sy ’na, Collwyn,’ meddai Hagrid mewn rhyddhad. ‘Sut \hlti{wyt ti}?’}
}

\noindent
In \cref{ex:hidden cloak} fantasy elements play a role. Harry, Hermione and Ron are hiding beneath the Invisibility Cloak, but the poltergeist Peeves can sense them even though he cannot see them.%
\footnote{It is noteworthy that \C{chi} is used in apposition with the singular forms \C{ellyll} ‘goblin, elf, bogey,~…’, \C{ysbryd} ‘spirit, ghost, spectre’ and \C{disgybl} ‘student, pupil’. This shows that the use of \C{chi} does not imply the speaker senses there is more than one person hidden, but it is used an unmarked form in such circumstances.}

\example{16}{199}{217}{}{hidden cloak}
{
%	\bi
%	{They didn’t meet anyone else until they reached the staircase up to the third floor. Peeves was bobbing halfway up, loosening the carpet so that people would trip.}
%	{Welson nhw neb arall nes iddyn nhw gyrraedd y grisiau a arweiniai i’r trydydd llawr. Roedd y Piwsiwr yn sboncio o gwmpas hanner y ffordd i fyny, yn llacio’r carped er mwyn i bobl faglu.}
%
	\bi
	{‘Who’s there?’ he said suddenly as they climbed towards him. He narrowed his wicked black eyes. ‘Know \hlyou{you}’re there, even if I can’t see \hlyou{you}. Are \hlyou{you} ghoulie or ghostie or wee student beastie?’}
	{‘Pwy sy ’na?’ meddai’n sydyn wrth iddyn nhw ddringo tuag ato. Culhaodd ei lygaid duon maleisus. ‘Wn i \hlchiunknown{eich} bod \hlchiunknown{chi} yna, hyd yn oed os na fedra i \hlchiunknown{eich} gweld \hlchiunknown{chi}. Be \hlchiunknown{ydych chi}, ellyll neu ysbryd neu ddisgybl?’}
}



\subsubsection{Non-specific readers}

Another type of cases that demonstrate this use of \C{chi} is addressing someone undefined in writing.
Such cases can be either in written words within the book’s world (\cref{ex:reader book titles,ex:reader puzzle})%
\footnote{%
	This is different from the aforementioned impersonal or generic use of second person markers, which are not referential.
	Here the second person markers are referential, but the reference is not defined and limited to a specific person.
	It is similar to signs that command the reader to do something or to avoid doing it, such as \C{cadwch oddi ar y glaswellt} ‘keep off the grass’ (\C{cadwch} being the imperative \C{chi}-form of \C{cadw} ‘to keep’).
}
or by the author as a literary device (\cref{ex:author-readers,ex:FIS}).

The address in \cref{ex:reader book titles}, written in the book’s lengthy sub-title, is directed towards any potential reader.

\example{4}{39}{36}{}{reader book titles}
{
	\bi
	{Hagrid almost had to drag Harry away from \emph{Curses and Counter-Curses (Bewitch \hlyou{your} Friends and Befuddle \hlyou{your} Enemies with the Latest Revenges: Hair Loss, Jelly-Legs, Tongue-Tying and much, much more)} by Professor Vindictus Viridian.}
	{Bu raid i Hagrid fwy neu lai lusgo Harri oddi wrth \emph{Melltithion a Gwrthfelltithion (\hlchiunknown{Rheibiwch} \hlchiunknown{Eich} Ffrindiau a \hlchiunknown{Dryswch} \hlchiunknown{Eich} Gelynion Drwy Dalu’r Pwyth yn ôl: Colli Gwallt, Coesau Jeli, Clymu Tafod a llawer iawn, iawn mwy)} gan yr Athro Daniel Dialydd.}

	%\bi
	%{‘I was trying to find out how to curse Dudley.’}
	%{‘Ceisio darganfod sut y medra i felltithio Dudley oeddwn i.’}
}

\noindent
In \cref{ex:reader puzzle} whoever will read the written riddle is addressed.

\example{16}{206}{226}{}{reader puzzle}
{
	%\bi
	%{‘Look!’ Hermione seized a roll of paper lying next to the bottles. Harry looked over her shoulder to read it:}
	%{‘Edrych!’ Cythrodd Hermione i rolyn o bapur wrth ochr y poteli. Edrychodd Harri dros ei hysgwydd i’w ddarllen:}
	%
	\bi
	{%
		\emph{Danger lies before \hlyou{you}, while safety lies behind,}~/
		\emph{Two of us will help \hlyou{you}, whichever \hlyou{you} would find,}~/
		\emph{One among us seven will let \hlyou{you} move ahead,}~/
		[…]
	}
	{%
		\emph{\hlchiunknown{O’ch} blaen mae perygl, ond mae’n ddiogel tu cefn,}~/
		\emph{Bydd dwy ohonom yn help, ond \hlchiunknown{ichi} weld y drefn,}~/
		\emph{Gadael \hlchiunknown{ichi} symud ymlaen wnaiff un ymysg saith,}~/
		[…]
	}
}

The author in the corpus never addresses the actual readers in the Narrator’s Channel%
\footnote{See \textcite[§~2.5]{eshel.o:2015:narrative} and \textcites[pp.~233–235]{shisha-halevy.a:1998:roberts}[§1.3]{shisha-halevy.a:2011:worknotes}.}
with referential second person markers (for example, providing meta-narrative information: ‘The story you are about to read is true’).
What is common, though, is non-referential impersonal (generic) use of second person markers by the author, as a literary technique.
This is demonstrated in \cref{ex:author-readers}.

\example{10}{130}{139}{}{author-readers}
{
	\bi
	{
		%Harry then did something that was both very brave and very stupid: he took a great running jump and managed to fasten his arms around the troll’s neck from behind.
		[…] The troll couldn’t feel Harry hanging there, but even a troll will notice if \hlyou{you} stick a long bit of wood up its nose, and Harry’s wand had still been in his hand when he’d jumped~— it had gone straight up one of the troll’s nostrils.
	}
	{
		%Yna gwnaeth Harri rywbeth oedd yn ddewr iawn a hefyd yn andros o ffôl: rhedodd at yr ellyll gan neidio a llwyddo i daflu ei freichiau amdano o’r tu cefn.
		[…] Fedrai’r ellyll ddim teimlo Harri’n hongian yno, ond bydd hyd yn oed ellyll yn sylwi os \hlchinarrative{gwthiwch chi} ddarn hir o bren i fyny’i drwyn, ac roedd hudlath Harri yn ei law tra oedd o’n neidio~— ac wedi mynd yn syth i fyny un o ffroenau’r ellyll.
	}
}

Another literary technique~— free indirect speech~— is demonstrated in \cref{ex:FIS}.
This example is telling, because if one turns the pages back to when Hagrid actually said the words in question, he was speaking familiarly with Harry, in \cref{ex:re-FIS}.
The use of \C{chi} here does not mirror the original wording, as if in a quote, but conforms to the norm of a literary technique.
In addition to the \C{ti:chi} difference, the spelling is standardized and the tense is different.

\example{9}{120}{128}{}{FIS}
{
	\bi
	{
		%But Hermione had given Harry something else to think about as he climbed back into bed.
		[…] The dog was guarding something~… What had Hagrid said? Gringotts was the safest place in the world for something \hlyou{you} wanted to hide~— except perhaps Hogwarts.
	}
	{
		%Ond roedd Hermione wedi rhoi rhywbeth arall i Harri feddwl amdano wrth iddo ddringo’n ôl i’w wely.
		[…] Roedd y ci’n gwarchod rhywbeth… beth oedd Hagrid wedi ei ddweud? Banc Gringrwn oedd y lle mwyaf diogel yn y byd ar gyfer rhywbeth \hlchinarrative{roeddech chi} eisiau’i guddio~— heblaw Hogwarts, efallai.
	}
}


\example{5}{50}{49}{}{re-FIS}
{
	\bi
	{
		%‘Yeah~— so yeh’d be mad ter try an’ rob it, I’ll tell yeh that.
		[…] Never mess with goblins, Harry. Gringotts is the safest place in the world fer anything \hlyou{yeh} want ter keep safe~— ’cept maybe Hogwarts.~[…]
	}
	{
		%‘Ia~— felly fe fyddat ti’n gwbl wallgo i geisio dwyn o’no, dwi’n deud ’that ti.
		[…] Paid byth â thynnu coblyn yn dy ben, Harri. Banc Gringrwn ydi’r lle mwya diogal yn y byd i gyd ar gyfar rhwbath \hlti{rwyt ti} angan ei warchod~— heblaw Hogwarts, ella.~[…]
	}
}
