\subsection{Shift in address form}
\label{sec:shift}

One immediate result of compiling the said table is pairs of \textsf{speaker} and \textsf{addressee} that occur with a different \textsf{address form} in different rows.
As the relationships between characters are not set in stone, a change in the relationship can be reflected in shifting from one address form to the other or be signalled by it \parencite[see][]{pavesi.m:2012:enriching}.

For example, when the child protagonist Harry meets the character Hagrid, the latter is a grown-up man whom he doesn’t know.
It is not surprising, then, that he addresses him at first using the polite \C{chi} in \cref{ex:Harry-Hagrid-A}, since it is usual for children to address unknown grown-ups so.
But as Hagrid tells his story and the close connection between them is revealed, Harry shifts to using the familiar \C{ti} in \cref{ex:Harry-Hagrid-B}, which he continues to do throughout the book.
Hagrid’s friendly tone and behaviour towards Harry and his colloquial speech contribute to the shift.

\example{4}{40}{37}{}{Harry-Hagrid-A}
{
	\bi
	{Harry looked up at the giant. He meant to say thank you, but the words got lost on the way to his mouth, and what he said instead was, ‘Who are \hlyou{you}?’}
	{Edrychodd Harri i fyny ar y cawr. Bwriadai ddweud diolch, ond aeth y geiriau ar goll ar y ffordd i’w geg, a’r hyn a ddywedodd o oedd, ‘Pwy \hlchisg{’dach chi}?’}
}

\example{4}{46}{44}{}{Harry-Hagrid-B}
{
	%\bi
	%{Harry, meanwhile, still had questions to ask, hundreds of them.}
	%{Yn y cyfamser, roedd gan Harri gwestiynau i’w gofyn, cannoedd ohonyn nhw.}
	%
	\bi
	{‘But what happened to Vol— sorry~— I mean, \hlyou{You}-Know-Who?’}
	{‘Ond beth ddigwyddodd i Vol~— ddrwg gen i~— \hlti{Wyddost-Ti}-Pwy, dwi’n feddwl?’}
}

Hagrid is a very interesting character sociopragmatically, as he stands somewhere between the teachers and the students in the social structure of Hogwarts, the school in which the book takes place.
Therefore, it is not surprising that as the special mixed-age clique%
\footnote{See \textcite[288–289]{jentsch.n.k:2002:tower-babel} for a discussion of this clique with regards to the T-V distinction in the French, German and Spanish translations.}
of Hagrid and the students Harry, Hermione and Ron develops over time Hermione starts using \C{ti} towards Hagrid as well.
In \cref{ex:Hermione-Hagrid-A} she addresses him with \C{chi}, but three chapters later, in \cref{ex:Hermione-Hagrid-B}, she begins to address him with \C{ti}.
The choice of \C{ti} here is affected by the context of this utterance as well:
Hermione uses it as a rhetorical device in flattering, linguistically signalling closeness%
\footnote{Akin to \posscite{brown.r+:1960:power-solidarity} \emph{solidarity}, signalled by reciprocal use of T-V pronouns.},
in the hope it will help her to extract the information from him. Once the shift is done, she continues to address him with \C{ti} from then on.

\example{11}{141}{151}{}{Hermione-Hagrid-A}
{
%	\bi
%	{The afternoon’s events certainly seemed to have changed her mind about Snape.}
%	{Roedd digwyddiadau’r pnawn yn amlwg wedi newid ei meddwl ynghylch Sneip.}
%
	\bi
	{‘I know a jinx when I see one, Hagrid, I’ve read all about them! \hlyou{You}’ve got to keep eye contact, and Snape wasn’t blinking at all, I saw him!’}
	{‘Dwi’n nabod melltith pan wela i un, Hagrid. Dwi wedi darllen amdanyn nhw! Mae’n rhaid \hlchisg{ichi} gadw cyswllt llygad, a doedd amrannau Sneip ddim yn symud o gwbl. Welais i o!’}
}

\example{14}{169}{184}{}{Hermione-Hagrid-B}
{
	\bi
	{‘Oh, come on, Hagrid, \hlyou{you} might not want to tell us, but \hlyou{you} \emph{do} know, \hlyou{you} know everything that goes on round here,’ said Hermione in a warm, flattering voice. Hagrid’s beard twitched and they could tell he was smiling.}
	{‘O, \hlti{ty’d} ’laen, Hagrid, ella nad \hlti{wyt ti} ddim isio dweud wrthon ni, ond \hlti{\emph{rwyt} ti}’n gwybod. \hlti{Rwyt ti}’n gwybod \emph{popeth} sy’n digwydd yn y lle yma,’ meddai Hermione, a’i llais yn fêl i gyd. Sbonciodd barf Hagrid. Roedd yn ddigon hawdd dweud ei fod yn gwenu.}
}

Not all \C{chi} to \C{ti} shifts signal the same kind of interpersonal relation shift.
The formally analogous transition when Vernon Dursley, Harry’s adoptive father, speaks to Hagrid bears a completely different meaning.
In \cref{ex:Vernon-Hagrid-A}, after Hagrid enters a shack where the Dursleys and Harry stay, Vernon keeps distance from Hagrid at first and politely~— yet decisively~— demands him to leave.
Hagrid stays and talks with Harry.
The moment Hagrid is about to tell Harry the secret Vernon fears the most (that Harry is a wizard and his parents, who were wizards as well, were killed magically), all politeness is gone and Vernon’s speech turns harsh and straightforward in \cref{ex:Vernon-Hagrid-B}, in which \C{taw} ‘be silent!’ and \C{paid} ‘don’t!’ are imperative \C{ti}-forms (contrasting with the imperative \C{chi}-forms \C{tewch} and \C{peidiwch}).

\example{4}{40}{37}{}{Vernon-Hagrid-A}
{
	\bi
	{Uncle Vernon made a funny rasping noise.}
	{Gwnaeth Yncl Vernon sŵn crafu rhyfedd.}

	\bi
	{‘I demand that \hlyou{you} leave at once, sir!’ he said. ‘\hlyou{You} are breaking and entering!’}
	{‘Dwi’n mynnu \hlchisg{eich} bod \hlchisg{chi}’n gadael ar unwaith!’ meddai. ‘\hlchisg{Rydych chi}’n torri’r gyfraith!’}
}

\example{4}{41}{39}{}{Vernon-Hagrid-B}
{
%	\bi
%	{‘But yeh must know about yer mum and dad,’ he said. ‘I mean, they’re famous. You’re famous.’}
%	{‘Ond ma’n rhaid dy fod ti’n gwbod rwbath am dy fam a dy dad,’ meddai. ‘Hynny ydi, ma’n nhw’n \emph{enwog}. Rwyt \emph{ti’n} enwog.’}
%
%	\bi
%	{‘What? My~— my mum and dad weren’t famous, were they?’}
%	{‘Be? Doedd~— fy mam a ’nhad i ddim yn enwog, oedden nhw?’}
%
%	\bi
%	{[…]}
%	{[…]}
%
%	%\bi
%	%{‘Yeh don’ know~… yeh don’ know~…’ Hagrid ran his fingers through his hair, fixing Harry with a bewildered stare.}
%	%{‘Wyddost ti ddim… wyddost ti ddim…’ Tynnodd Hagrid ei fysedd drwy’i wallt, gan rythu’n ddryslyd ar Harri.}
%
%	\bi
%	{‘Yeh don’ know what yeh are?’ he said finally.}
%	{‘Wyddost ti ddim be \emph{wyt} ti?’ meddai o’r diwedd.}
%
%	\bi
%	{Uncle Vernon suddenly found his voice.}
%	{Yn sydyn cafodd Yncl Vernon hyd i’w lais.}
%
	\bi
	{‘Stop!’ he commanded. ‘Stop right there, sir! I forbid \hlyou{you} to tell the boy anything!’}
	{‘\hlti{Taw}!’ gorchmynnodd. ‘\hlti{Taw}’r munud yma! \hlti{Paid} â meiddio dweud dim byd wrth yr hogyn!’}
}

If previous examples demonstrated pronoun shift in connection with change in relationship, \cref{ex:Firenze-Harry} is of a different kind.
At first Firenze the centaur thinks the child in front of him is a random Hogwarts student; therefore, he addresses him with \C{ti}, which is the usual form for adults to address children.
But when he realizes it is actually the famous Harry Potter he is speaking with he shifts to \C{chi}, showing him respect (see~\cref{sec:chi-ing Harry}).
This change, then, results from not recognizing the addressee correctly at first.

\example{15}{187}{203}{}{Firenze-Harry}
{
	\bi
	{‘Are \hlyou{you} all right?’ said the centaur, pulling Harry to his feet.}
	{‘\hlti{Wyt ti}’n iawn?’ gofynnodd y dynfarch, gan godi Harri ar ei draed.}

%	\bi
%	{‘Yes~— thank \hlyou{you}~— what was that?’}
%	{‘Ydw~— diolch~— be \emph{oedd} hwnna?’}

	\bi
	{[…]}
	{[…]}

	\bi
	{%The centaur didn’t answer. He had astonishingly blue eyes, like pale sapphires.
		[…]
	He looked carefully at Harry, his eyes lingering on the scar which stood out, livid, on Harry’s forehead.}
	{%Atebodd y dynfarch ddim. Roedd ganddo lygaid gleision syfrdanol, fel saffir gwelw,
		[…]
	a chraffodd ar Harri, gan oedi ar y graith ddulas, gleisiog ar ei dalcen.}

	\bi
	{‘\hlyou{You} are the Potter boy,’ he said. ‘\hlyou{You} had better get back to Hagrid. The Forest is not safe at this time~— especially for \hlyou{you}. Can \hlyou{you} ride? It will be quicker this way.}
	{‘Harri Potter \hlchisg{ydych chi},’ meddai, ‘Well \hlchisg{ichi} fynd yn ôl at Hagrid. Dydi’r Goedwig ddim yn ddiogel ar yr adeg yma~— yn arbennig i \hlchisg{chi}. \hlchisg{Fedrwch chi} farchogaeth? Bydd yn gyflymach fel hyn.}
}
