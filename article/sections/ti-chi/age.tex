\subsection{Age and status}
\label{sec:age}

\begin{samepage} % So that the table doesn’t go between the paragraph and the list
Belonging to the boarding school story genre, the corpus offers an opportunity to examine the linguistic expression of social relationships in which age and status play a major role. These topics are discussed:

\begin{itemize}
	\item social relations within the school (\cref{sec:students and teachers}),
	\item different kinds of family units (\cref{sec:families}),
	\item the special place Harry Potter has within the wizard community (\cref{sec:chi-ing Harry}).
\end{itemize}
\end{samepage}



\subsubsection{Students and teachers}
\label{sec:students and teachers}

\Cref{tab:students and teachers} summarizes interactions between students and teachers.
Without exceptions, students address teachers with \C{chi} and each other with \C{ti}.
Staff members of equal status address each other with \C{ti} and their superiors with \C{chi}: McGonagall and Dumbledore are Deputy Headmistress and Headmaster, respectively, Snape and Quirrell are both teachers.

\begin{table}
	\centering
	\begin{tabular}{rlll}
		\toprule
        & speaker      & addressee       & T-V     \\
		\midrule
        & student     & teacher                 & \C{chi} \\
        & student     & student                 & \C{ti}  \\
		\ldelim\{{5}{*}[\crotatebox{90}{\footnotesize teacher{\toarrow}teacher}]
        & Dumbledore  & McGonagall              & \C{ti}  \\
        & McGonagall  & Dumbledore              & \C{ti}  \\
        & Quirrell    & Dumbledore              & \C{chi} \\
        & Quirrell    & Snape                   & \C{ti}  \\
        & Snape       & Quirrell                & \C{ti}  \\
		\ldelim\{{7}{*}[\crotatebox{90}{\footnotesize teacher{\toarrow}student}]
        & Dumbledore  & Harry                   & \C{ti}  \\
        & Madam Hooch & Neville                 & \C{ti}  \\
        & McGonagall  & \Stack{Draco, Harry, Hermione, \\ Jordan, Neville, Wood} & \C{chi}     \\
        & Quirrell    & Harry                   & \C{chi} \\
        & Snape       & Harry, Hermione, Seamus & \C{chi} \\
        & Snape       & Neville                 & \C{ti}  \\
		\bottomrule
	\end{tabular}
	\caption{Address forms between students and teachers}
	\label{tab:students and teachers}
\end{table}

The address forms teachers use when speaking to students show diversity:
in most cases students are addressed with \C{chi}, while in two cases Neville is addressed with \C{ti} and Harry is regularly addressed so by Dumbledore.
The \C{chi} cases are discussed first, followed \C{ti} ones.



\paragraph{Teachers addressing students with \C{chi}}

\noindent
The fact teachers use \C{chi} when speaking to students might seem unexpected, since students rank lower in the school hierarchy.
According to \textcite[§~4.130]{thomas.p:2006:gramadeg} in the 1960s students and teachers used \C{chi} reciprocally\, and this was changed later so that an irreciprocal relationship is now more common.
One possible explanation for this reciprocal use of \C{chi} is that it signals social distance, even though the difference in power is excepted \foreign{a priori} to cause irreciprocal use \parencite[see][]{brown.r+:1960:power-solidarity}.
So, since it seems that the main \emph{teacher \toarrow\ student} address form represents the use of the 1960s while the book is set in the 1990s, it raises a question concerning linguistic anachronism.
\textcite[84]{pritchard.ff.h:2014:cyfieithu-tair} suggests this has to do with Hogwarts being a boarding school, and as such it is more formal than common British schools.
Two other factors could contribute to this choice as well:
one is that this is what the translator, who was born in 1942, is used to from her days at school;
the other is that Hogwarts, being an old-fashioned school in every aspect, is linguistically presented as such.
%Non-teacher staff usually use \C{ti} towards students.

Examining \cref{ex:ylw} first can facilitate understanding \cref{ex:Quirrell-Harry-B}.
The situation depicted in this example~— which is from another corpus, \textcite{roberts.k:1960:lon-wen}~— took place at the beginning of the \nth{20} century: a headmaster addressing a student with \C{chi} just before caning her.%
\footnote{According to the preceding and following passages, she was 10–15 years old (1901–1906) when it happened.}

\bareexample{Y Lôn Wen \parencite[{ch.~1}]{roberts.k:1960:lon-wen}; translation: Gillian Clarke \parencite{roberts.k:2009:white-lane}}{}{ylw}
{
	\bi
	{[…] mae un o’r bechgyn yn lluchio pysen tuag ataf. Try’r ysgolfeistr yn ei ôl a gofyn i mi yn Saesneg pwy a’i taflodd. Dywedaf na wn, a rhoi fy nwy wefus ar ei gilydd yn dynn. ‘Fe \hlchisg{ddylech} wybod,’ medd ef, a rhoi dwy gansen gïaidd i mi, un ar bob llaw, mor galed ag y gall. Ond nid wyf yn crïo. Deil fy ngwefusau yn dynn ar ei gilydd.}
	{[…] one of the boys lobs a pea at me. The headmaster turns back and asks me in English who threw the pea. I say I don’t know, and keep my lips tight together. ‘\hlyou{You} should know,’ he says, and he gives me two fierce strokes of the cane, one on each hand, with all his might. But I don’t cry. I keep my lips tight shut.}	
}

\noindent
Kindness and politeness, then, are two independent things, on two different planes.
This can help one understand why Quirrell, the Defence Against the Dark Arts teacher, continues to address Harry with \C{chi} even after being revealed as a villain in \cref{ex:Quirrell-Harry-B}.
In the same scene Voldemort, the antagonist, addresses Harry with \C{ti} in \cref{ex:Voldemort-Harry}, but he is not Harry’s teacher nor does he wish to show him any respect.

\example{17}{209}{229}{}{Quirrell-Harry-B}
{
%	\bi
%	{It was Quirrell.}
%	{Quirrél oedd o.}
%
%	\bi
%	{‘\emph{You!}’ gasped Harry.}
%	{‘\emph{Chi!}’ ebychodd Harri.}
%
%	\bi
%	{Quirrell smiled. His face wasn’t twitching at all.}
%	{Gwenodd Quirrél. Doedd ei wyneb ddim yn plycio o gwbl.}
%
	\bi
	{‘Me,’ he said calmly, ‘I wondered whether I’d be meeting \hlyou{you} here, Potter.’}
	{‘Fi,’ meddai’n ddigyffro. ‘Ro’n i’n meddwl tybed a fyddwn i’n \hlchisg{eich} cyfarfod \hlchisg{chi} yma, Potter.’}
}

\example{17}{213}{233}{}{Voldemort-Harry}
{
	%\bi
	%{‘Harry Potter…’ it whispered.}
	%{‘Harri Potter…’ sibrydodd.}
	%
	%\bi
	%{Harry tried to take a step backwards but his legs wouldn’t move.}
	%{Ceisiodd Harri gamu’n ôl ond gwrthodai ei goesau symud.}
	%
	\bi
	{%‘See what I have become?’ the face said. ‘Mere shadow and vapour… I have form only when I can share another’s body… but there have always been those willing to let me into their hearts and minds…
	‘[…] Unicorn blood has strengthened me, these past weeks… \hlyou{you} saw faithful Quirrell drinking it for me in the Forest… and once I have the Elixir of Life, I will be able to create a body of my own… Now… why don’t \hlyou{you} give me that Stone in \hlyou{your} pocket?’}
	{%‘Weli di beth sydd wedi digwydd imi?’ meddai’r wyneb. ‘Tarth a chysgod yn unig ydw i bellach. Ffurfiaf gorff yn unig pan gaf rannu un rhywun arall. Bu rhai bob amser yn ddigon parod i’m croesawu i’w calonnau a’u meddyliau.
	‘[…] Yr wythnosau diwethaf yma fe’m cryfhawyd gan waed uncorn… \hlti{welaist ti} Quirrél ffyddlon yn ei yfed i mi yn y Goedwig… ac unwaith y bydd Elicsir Bywyd gen i, byddaf yn medru creu fy nghorff fy hun… Nawr… pam na \hlti{roi di}’r Maen yna sydd yn \hlti{dy} boced i mi?’}
}



\paragraph{Teachers addressing students with \C{ti}}

\noindent
Neville is addressed with \C{ti} by teachers in two situations: by Madam Hooch the flying teacher in \cref{ex:Hooch-Neville} and by Professor Snape the potions teacher in \cref{ex:Snape-Neville}.
In both examples this happens just after he acts very clumsily, which resulted in him ending up in the hospital wing and causing mayhem in class.
Therefore, it is quite understandable why these teachers do not address him using \C{chi} in these particular situations.
The choice of pronominal address forms is reinforced by nominal ones in both cases, reflecting the difference in the two teacher’s attitude:
Madam Hooch calls Neville \emph{boy} (\C{hogyn}) and \emph{dear} (\C{’ngwas i}, not quoted here), endearingly;
Professor Snape on the other hand calls him \emph{idiot boy} (\C{hogyn hurt}), disaffectionately\footnote{Compare this with commanding Seamus, at whom he is not so angry, using a \C{chi}-form (\C{ewch}): \C{Ewch ag o i’r ysbyty} ‘Take him up to the hospital wing’.}.

\example{9}{109}{116}{}{Hooch-Neville}
{
%	\bi
%	{But Neville, nervous and jumpy and frightened of being left on the ground, pushed off hard before the whistle had touched Madam Hooch’s lips.}
%	{Ond gan fod Nefydd yn nerfus ac yn ofni cael ei adael ar y ddaear, gwthiodd i ffwrdd yn gryf cyn i’r bib gyffwrdd gwefusau Madam Heddwen.}
%
	\bi
	{‘Come back, boy!’ she shouted, but Neville was rising straight up like a cork shot out of a bottle […]}
	{‘\hlti{Ty’d} yn ôl, hogyn!’ gwaeddodd, ond roedd Nefydd yn codi’n syth i fyny fel corcyn yn saethu allan o botel […]}

	\bi
	{[…]}
	{[…]}

	\bi
	{‘Broken wrist,’ Harry heard her mutter. ‘Come on, boy~— it’s all right, up \hlyou{you} get.’ }
	{‘Wedi torri’i arddwrn,’ clywodd Harri hi’n mwngial. ‘\hlti{Ty’d} ’laen, hogyn~— popeth yn iawn, \hlti{cod} ar \hlti{dy} draed.’}
}

\example{8}{103}{109}{}{Snape-Neville}
{
%	\bi
%	{[…] Neville had somehow managed to melt Seamus’s cauldron into a twisted blob and their potion was seeping across the stone floor, burning holes in people’s shoes. […]}
%	{[…] Rywfodd, roedd Nefydd wedi llwyddo i doddi crochan Seamus yn llanast meddal, a llifai eu dracht ar hyd y llawr, gan losgi tyllau yn esgidiau pobl. […]}
%
	\bi
	{‘Idiot boy!’ snarled Snape, clearing the spilled potion away with one wave of his wand. ‘I suppose \hlyou{you} added the porcupine quills before taking the cauldron off the fire?’}
	{‘Hogyn hurt!’ chwyrnodd Sneip, gan glirio’r llanast ag un chwifiad o’i hudlath. ‘Mae’n debyg \hlti{iti} ychwanegu’r pigau porciwpein cyn tynnu’r crochan oddi ar y tân.’}

	\bi
	{Neville whimpered as boils started to pop up all over his nose.}
	{Griddfanodd Nefydd yn gwynfanllyd wrth i gornwydydd ddechrau ffurfio dros ei drwyn.}

	\bi
	{‘Take him up to the hospital wing,’ Snape spat at Seamus. […]}
	{‘\hlchisg{Ewch} ag o i’r ysbyty,’ poerodd Sneip at Seamus. […]}
}

Dumbledore the Headmaster speaks with Harry on three occasions.
In these occasions they speak alone (or quietly enough so no one else can hear them) and not in a class or class-like situation.
Dumbledore addresses Harry with more than sixty occurrences of pronominal address, all of which using \C{ti}.
This systematic choice possibly signals a special closeness, not considering Harry as any other Hogwarts student to be addressed by the distancing \C{chi}.
This accords with the fact that Dumbledore in the original English text calls him \emph{Harry} throughout the book, while the teachers (Flitwick, McGonagall, Snape and Quirrell) call him \emph{Potter}.
The distinction between first name address and last name address has sociolinguistic similarities with T-V distinctions.
This characteristic of Dumbledore is but one of his peculiarities, some of which are linguistic.



\subsubsection{Children and their (adoptive) parents}
\label{sec:families}

In addition to the difference between generations regarding the use of \C{ti} and \C{chi} in schools (\cref{sec:students and teachers}), \textcite{thomas.p:2006:gramadeg} presents in the same passage a development in the opposite direction:
there is a strong tendency for reciprocal use of \C{ti} between parents and children in contemporary%
\footnote{\textcite{thomas.p:2006:gramadeg} was first published in 1996.}
Welsh, whereas in older generations it was common for parents to speak to their children with \C{ti} and to receive \C{chi}. 

Two families are represented speaking in the text:
the Dursley family (mother Petunia, father Vernon, their biological son Dudley and their adopted son Harry Potter) and
the Weasley family (mother Molly and her children talk a little at the train station).
Their use of \C{ti} and \C{chi} differs, and represents two distinct types of families.

Molly Weasley addresses her children with \C{ti} and they address her the same way, as demonstrated in \cref{ex:Fred=Molly}.

\example{6}{70}{72}{}{Fred=Molly}
{
	\bi
	{‘Fred, \hlyou{you} next,’ the plump woman said.}
	{‘Fred, \hlchdi{chdi} nesa,’ meddai’r wraig nobl.}

	\bi
	{‘I’m not Fred, I’m George,’ said the boy. ‘Honestly, woman, call \hlyou{yourself} our mother? Can’t \hlyou{you} tell I’m George?’}
	{‘George ydw i, nid Fred,’ meddai’r bachgen. ‘A \hlti{tithau}’n \hlti{dy} alw \hlti{dy} hun yn fam inni, wir! \hlti{Wyddost ti} ddim mai George ydw i?’}
%
%	\bi
%	{‘Sorry, George, dear.’}
%	{‘Ddrwg gen i, George, ’ngwas i.’}
%
%	\bi
%	{‘Only joking, I am Fred,’ said the boy, and off he went. His twin called after him to hurry up, […]}
%	{‘Pryfocio o’n i! Fred ydw i,’ meddai’r bachgen, ac i ffwrdd ag o. Galwodd ei efaill arno i frysio, […]}
}

The Dursley(-Potter) family shows a different, split relationship between its members.
This is elegantly demonstrated in \cref{ex:Vernon-Dudley-Harry}, which shows concisely how
Vernon talks with Dudley (\C{ti}, \nth{1} line),
Dudley with Vernon (\C{ti}, \nth{2} line),
Vernon with Harry (\C{ti}, \nth{3} line) and
Harry with Vernon (\C{chi}, \nth{4} line);
see also \textcite[83]{pritchard.ff.h:2014:cyfieithu-tair}.

\example{3}{29}{26}{}{Vernon-Dudley-Harry}
{
	\bi
	{‘Get the post, Dudley,’ said Uncle Vernon from behind his paper.}
	{‘\hlti{Cer} i nôl y post, Dudley,’ meddai Yncl Vernon o’r tu cefn i’w bapur.}

	\bi
	{‘Make Harry get it.’}
	{‘\hlti{Gwna} i Harri ei nôl o.’}

	\bi
	{‘Get the post, Harry.’}
	{‘\hlti{Cer} i nôl y post, Harri.’}

	\bi
	{‘Make Dudley get it.’}
	{‘\hlchisg{Gwnewch} i Dudley ei nôl o.’}
}

\noindent
This split parent-child relationship is a linguistic expression of the unloving attitude of the Dursleys towards their adopted son as opposed to their loving one towards their biological son.
Treating Harry not as a full member of the family can be seen in everything the Dursleys do \parencite[see][45]{lavoie.c:2003:houses}.%
\footnote{In fact, Vernon explicitly says so at the very end of the book:
	when Molly Weasley meets the Dursleys and says ‘You must be Harry’s family!’ \C{(Chi ydi teulu Harri, mae’n rhaid!)} Vernon replies ‘In a manner of speaking’ \C{(Mewn ffordd o siarad)}.
}
While the traditional irreciprocal use of \C{ti} and \C{chi} within the family (\posscite{brown.r+:1960:power-solidarity} \emph{power} relation) continues to this day to some extent,%
\footnote{I would like to thank Samuel Jones for informing me about this (personal communication, 2018).}
this is not the case with the Dursleys: it is the \emph{differentiation} between children in the same household that essentially alienates Harry, not the irreciprocal use \foreign{per se} (if Dudley were to speak with \C{chi} to his parents as well, there would be no differentiation and alienation).



\subsubsection{Harry Potter’s place within the wizard community}
\label{sec:chi-ing Harry}

The first part of the book revolves around one main theme:
how Harry~— a neglected, abused and unpopular child in the ‘Muggle’ (non-magical) world~— turns up to be an admired wizard in the magical community, in a manner not unlike Andersen’s \worktitle{Ugly Duckling}.
The respect people show Harry, who has defeated the dreaded Voldemort (albeit unknowingly), is expressed both by literary and linguistic means.
This has been demonstrated earlier in \cref{ex:Firenze-Harry} (\cref{sec:shift}), where Firenze shifts from \C{ti} to \C{chi} once he realizes he is speaking with Harry.

The transformation is most pronounced at the Leaky Cauldron pub, the portal into the Wizarding World, in a scene where many strangers come and greet Harry when he arrives there, delighted to see him, a scene whose main literary purpose is to highlight the difference in attitude towards the protagonist between the two worlds.
In the English text they address him as \emph{Mr Potter}%
\footnote{%
	This syntagm, \emph{Mr Potter}, is used quite sparingly in the book:
	by people who wish to emphasize their respect for him (people in that pub, Firenze and Ollivander (see \cref{ex:Ollivander-Harry})
	and by Professor McGonagall in a formal letter and when she is very cross with him (using it as a distancing form).
}
(\C{y Bonwr Potter} in translation), and in the Welsh translation they use \C{chi} as well, as can be seen in \cref{ex:Leaky Cauldron}.
It should be noted that it is not usual for adults to address children with \C{chi} (the school situation discussed earlier is exceptional).

\example{5}{54}{54}{}{Leaky Cauldron}
{
%	\bi
%	{Then there was a great scraping of chairs and, next moment, Harry found himself shaking hands with everyone in the Leaky Cauldron.}
%	{Bu sŵn mawr crafu cadeiriau, a’r eiliad nesaf roedd pawb yn y Gogor-Grochan yn ysgwyd llaw â Harri.}
%
	\bi
	{‘Doris Crockford, Mr Potter, can’t believe I’m meeting \hlyou{you} at last.’}
	{‘Cadi Morgan, y Bonwr Potter. Fedra i ddim credu ’mod i’n \hlchisg{eich} cyfarfod \hlchisg{chi} o’r diwedd.’}

	\bi
	{‘So proud, Mr Potter, I’m just so proud.’}
	{‘Braint fawr, y Bonwr Potter, braint fawr.’}

	\bi
	{‘Always wanted to shake \hlyou{your} hand~— I’m all of a flutter.’}
	{‘Wedi dyheu erioed am gael ysgwyd llaw efo \hlchisg{chi}~— dwi wedi cynhyrfu’n lân!’}

	\bi
	{‘Delighted, Mr Potter, just can’t tell \hlyou{you}. Diggle’s the name, Dedalus Diggle.’}
	{‘Yn falch o’\hlchisg{ch} cyfarfod \hlchisg{chi}, y Bonwr Potter, yn falch dros ben. Dyfyr ydi’r enw, Dyfyr Drwyndwn.’}
}

After this scene Harry and Hagrid go to buy school equipment for Harry.
In \cref{ex:Malkin-Harry} it is demonstrated how a seller who does not recognize Harry addresses him with \C{ti}, while in \cref{ex:Ollivander-Harry} Ollivander the wand-maker does recognize him and addresses him with \C{chi}.
This use of \C{chi} towards Harry is highlighted in contrast with Hagrid in \cref{ex:Ollivander-Hagrid}, whom Ollivander does not hold in high esteem;
he addresses him with \C{ti}, not forgetting to remind him he was expelled from Hogwarts…

\example{5}{59}{60}{}{Malkin-Harry}
{
	\bi
	{‘Hogwarts, dear?’ she said, when Harry started to speak. ‘Got the lot here~— another young man being fitted up just now, in fact.’}
	{‘Hogwarts, ’ngwas i?’ meddai hi, cyn gynted ag yr agorodd Harri ei geg. ‘Mae’r cwbl gen i yn fan’ma~— a dweud y gwir mae ’na ŵr ifanc arall yn cael ei ffitio ar y funud hefyd.’}

	\bi
	{\textit{[a long conversation between Harry and Draco]}}
	{[…]}

	\bi
	{But before Harry could answer, Madam Malkin said, ‘That’s \hlyou{you} done, my dear,’ […]}
	{Ond cyn i Harri gael cyfle i ateb, meddai Malan Meirion, ‘Dyna \hlti{ti}’n barod, ’ngwas i,’ […]}
}

\example{5}{63}{64}{}{Ollivander-Harry}
{
%	\bi
%	{An old man was standing before them, his wide, pale eyes shining like moons through the gloom of the shop.}
%	{Safai hen ŵr o’u blaenau, ei lygaid llydan, gwelwon yn disgleirio fel lleuadau drwy fwrllwch y siop.}
%
%	\bi
%	{‘Hello,’ said Harry awkwardly.}
%	{‘Helô,’ meddai Harri’n chwithig.}
%
	\bi
	{‘Ah yes,’ said the man. ‘Yes, yes. I thought I’d be seeing \hlyou{you} soon. Harry Potter.’ It wasn’t a question. ‘\hlyou{You} have \hlyou{your} mother’s eyes. […]}
	{‘A, ie,’ meddai’r dyn. ‘Ie. Ie. Roeddwn i’n meddwl y byddwn i’n \hlchisg{eich} gweld \hlchisg{chi} cyn bo hir, Harri Potter.’ Nid cwestiwn oedd o. ‘Mae llygaid \hlchisg{eich} mam \hlchisg{gynnoch chi}. […]}
}

\example{5}{64}{65}{}{Ollivander-Hagrid}
{
	%\bi
	%{He shook his head and then, to Harry’s relief, spotted Hagrid.}
	%{Ysgydwodd ei ben ac yna, er rhyddhad i Harri, sylwodd ar Hagrid.}
	%
	\bi
	{‘Rubeus! Rubeus Hagrid! How nice to see \hlyou{you} again~… Oak, sixteen inches, rather bendy, wasn’t it?’}
	{‘Rubeus! Rubeus Hagrid! Dda gen i \hlti{dy} weld \hlti{di} eto… derw, un fodfedd ar bymtheg, eithaf ystwyth, yntê?’}

	\bi
	{‘It was, sir, yes,’ said Hagrid.}
	{‘Ia, syr, ia,’ meddai Hagrid.}

	\bi
	{‘Good wand, that one. But I suppose they snapped it in half when \hlyou{you} got expelled?’ said Mr Ollivander, suddenly stern.}
	{‘Hudlath dda, honna. Ond mae’n debyg iddyn nhw ei thorri hi’n glec yn ei hanner pan \hlti{gest ti} dy ddiarddel,’ meddai Onllwyn ab Oswallt, gan droi’n chwyrn yn sydyn.}
}
