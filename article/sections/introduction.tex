\section{Introduction}
\label{sec:introduction}

A quick look at the World Atlas of Language Structures (WALS) map in \textcite{helmbrecht.j:2013:wals-45} shows that politeness distinction in the second person is a phenomenon found across the globe.
In its binary form ‘familiar:polite’ (\grammar{fam:hon})%
\footnote{The terms \emph{familiar/informal} and \emph{polite/honorific/formal} seem to be used in free variation in the literature, sometimes even in the same publication.
My choice of \grammar{fam}:\grammar{hon} and \emph{familiar:polite} is after \textcite{helmbrecht.j:2013:wals-45}, for reasons of clarity, since WALS is used as a sort of a standard in typology.}
it is most common in Europe, an areal characteristic encompassing Indo-European languages as well as languages from other families.
This binary form is usually called a ‘T-V distinction’, after Latin \foreign{tū} and \foreign{vōs}, which evolved respectively into \grammar{2sg.fam} (cf.\ Fr.\ \foreign{tu}) and \grammar{2sg.hon}/\grammar{2pl} (cf.\ Fr.\ \foreign{vous}) in descendant languages (for general discussion regarding development and motivation, see \textcite{brown.r+:1960:power-solidarity}, \textcite[especially pp.~198–203 in §~5.4.4]{brown.p+:1987:politeness} and \textcite{helmbrecht.j:2006:hoeflichkeitspronomina}).
Welsh has such a T-V distinction; English, on the other hand, does not distinguish politeness in person marking.
This paper describes features of this distinction in a book translated into Welsh from English: \worktitle{Harri Potter a Maen yr Athronydd} \parencite{rowling.j:2003:harri_maen}, a translation of \worktitle{Harry Potter and the Philosopher’s Stone} (\cite{rowling.j:2004:hp1}; the first book in the \C{Harry Potter} series) made by Emily Huws, a children’s author from Caeathro.
When translating from English into Welsh, the translator was obliged to make politeness distinctions according to her understanding of the text in order to produce an idiomatic output.

The aim of this paper is double:

Its primary purpose is to describe qualitatively some of the particularities of the T-V distinction in Welsh, as reflected in the corpus.
Translated texts are to some degree different from other, ‘native’ varieties of language, such as original prose or spontaneous speech, but they have their own systematic linguistic features and constitute a valid and interesting variety of language.

Its secondary purpose is a more general one, to make first steps in a corpus-based typological cross-linguistic comparative project that makes use of \worktitle{Harry Potter and the Philosopher’s Stone} as a parallel text%
\footnote{See \worktitle{Sprachtypologie und Universalienforschung} 60.2 \parencite{stuf:2007:parallel}, an issue dedicated to linguistic analysis of parallel texts; in particular \textcite{cysouw.m+:2007:parallel-typology}, which introduces the notion ‘massively parallel text’ (a category of which the described corpus is a member \foreign{par excellence}, being translated into 74 languages in total), and \textcite{stolz.t:2007:potter-prince}, which deals with \worktitle{Harry Potter} in particular (§~3 there).}
in exploring differences and similarities in the sociopragmatic encoding of politeness and social relationships in second person markers in the world’s languages.
The utilization of a massively parallel single long text as the data differentiates it from similar projects like the Melbourne Address Projects\footnote{\url{https://arts.unimelb.edu.au/rumaccc/research/melbourne-address-projects}}.
Despite having drawbacks \parencite[see][]{walchli.b:2007:advantages-disadvantages} as data, a shared text in translation can facilitate systematic charting of what parameters matter across different languages (and to different translators) in the distribution of T and V.
All the more so when the corpus in question portrays diverse interpersonal relationships in numerous portions of dialogue.
The project is open research and the data files are freely available.%
\footnote{\label{fn:english.xml}%
	\ifthenelse{\equal{\copytype}{review}}
		{[Web address removed in the copy sent to review for the sake of anonymity]}
		{The tagged file for Welsh is available at \url{https://gitlab.com/rwmpelstilzchen/hp-tv/blob/master/research/welsh/english.xml}}
}

In recent years the question concerning the choice between T and V forms in translation has received scholarly attention in the context of film translations (either in dubbing or subtitling; AVT, audiovisual translation), investigating the translators’ strategies in providing additional sociopragmatic information when translating from languages without a T-V distinction into languages that do have it \parencite[e.g.][]{pavesi.m:2009:pronouns, levshina.n:2017:multivariate, pavesi.m:2012:enriching, meister.l:2016:dilemma}.

The topic of the T-V distinction in translations of \worktitle{Harry Potter} was touched upon in Welsh \parencite[§~4.2.3]{pritchard.ff.h:2014:cyfieithu-tair} as well as in other languages
(e.g. \textcite[288–290]{jentsch.n.k:2002:tower-babel} for French, German and Spanish,
\textcite[8]{wyler.l:2003:potter} for Brazilian Portuguese and
\textcite[466]{feral.a-l:2006:magic-wand} for French).
Nevertheless, to the best of my knowledge no in-depth inquiry was made into the topic this paper focusses on in the Welsh translation.



\subsection{The T-V distinction in the corpus}

Although Welsh is an Indo-European language whose main population is located in a European island, in many typological features it does not belong in the Standard Average European (SAE) \foreign{Sprachbund}, along with the other Celtic languages \parencite[see][]{haspelmath.m:2001:sae}.
Nevertheless, with regards to the sociopragmatic areal phenomenon in question Welsh behaves similarly to many of its neighbours in the continent \parencite{helmbrecht.j:2006:hoeflichkeitspronomina}: it has politeness distinction only in the \grammar{2sg}, the V-form (the polite form, \grammar{2sg.hon}) being homonymic with \grammar{2pl}.
The T-form (the familiar form, \grammar{2sg.fam}) is \C{ti} and the V-form is \C{chi}.%
\footnote{Their etymology is from their corresponding Proto Indo-European pronouns:
\C{ti}~< Middle Welsh \C{ti}~< Proto-Brythonic *\textit{ti}~< Proto-Celtic *\textit{tū}~< PIE *\textit{túh₂} (\grammar{2sg.nom}) and
\C{chi}~< \C{chwi}~< *\textit{hwi}~< \textit{*s{u\fontspec{Gentium} ̯}īs} (cf.\ Gaulish *\textit{suis})~< *\textit{wos}, *\textit{wēs} (\grammar{2pl.obl}).}
The T-V distinction occurs in all second person markers, dependent and independent alike, regardless of morphological form.

Thus, when encountering occurrences of English \E{you-} (\E{you}, \E{your}, etc.) the translator had to choose between several options, as illustrated in \cref{fig:choices} and explained below; see~\textcite{levy.j:1967:translation}.

\begin{figure}
	$$
	\mbox{\ruby{\E{you-}}{\grammar{2}} \quad\rightdashedarrow\quad}
	\begin{dcases}
		\quad\crotatebox{90}{\textsc{personal}}\quad
		\begin{dcases}
			\enspace\mbox{\grammar{2sg}}\enspace
			\begin{dcases}
				\enspace\mbox{\ruby{\textbf{\C{ti}}}{\grammar{2sg.fam}}}\\
				~\\
				\enspace\mbox{\ruby{\textbf{\C{chi}}}{\grammar{2sg.hon}}}
			\end{dcases}\\
			~\\
			\enspace\mbox{\grammar{2pl}}\enspace
			\begin{dcases}
				\enspace\mbox{\ruby{\C{chi}}{\grammar{2pl}}}
			\end{dcases}
		\end{dcases}\\
		\mbox{\textsc{(non-personal)}}
	\end{dcases}
	$$
	\caption{Translation choices}
	\label{fig:choices}
\end{figure}

In the vast majority of cases \E{you-} is translated by a second person marker:
\begin{itemize}
	\item if the speaker addresses one person, there exists a choice between \C{ti} (\grammar{2sg.fam}) and \C{chi} (\grammar{2sg.hon}).
		This choice is the one dealt with in this paper (\cref{sec:ti-chi}).
	\item if more than one person is addressed, there is no choice but \C{chi} (\grammar{2pl}).
\end{itemize}

As English and Welsh have different phraseology, idiomatics and grammar, in a minority of occurrences second person markers in the source text are not reflected by second person markers in the translation (‘\textsc{(non-personal)}’ in \cref{fig:choices}), and vice versa.
\cref{ex:you got,ex:angen,ex:rhywun,ex:thankyou,ex:tellyou} exemplify for the first direction:%
\footnote{%
	\noindent\begin{minipage}[t]{\textwidth} % in order to ensure it stays on the same page
		Technical notes concerning the examples:
		\begin{itemize}[leftmargin=2em]
			\item The page numbering indicates the page in the English text followed by the page in the Welsh text.
			\item In \cref{ex:you got,ex:angen,ex:rhywun,ex:thankyou,ex:tellyou} the grammatical structure is central for discussion, so limited glosses are provided.
				In the other examples, on the other hand, the choice between \C{ti} and \C{chi} is the most relevant piece of information.
				In order to make the paper accessible, simple typographical annotation is provided:
				\C{ti} is indicated in the Welsh text by bold letters (so: \hlti{arnat} ‘on you (\C{ti}-form)’), \C{chi} by bold small capital letters (so: \hlchisg{arnoch} ‘on you (\C{chi}-form)’);
				corresponding English \C{you-} is indicated by bold letters.
		\end{itemize}
\end{minipage}
}

In \cref{ex:you got} there is a difference in grammar, not using a second person marker in Welsh.

\example{12}{145}{155}{}{you got}
{
	\bi
	{‘How many days \hlyou{you} got left until yer holidays?’ Hagrid asked.}
	{‘\gl{Sawl}{how\_many} \gl{dwrnod}{day} \gl{eto}{yet} \gl{tan}{until} \gl{’ych}{\grammar{2pl.poss}} \gl{gwylia}{holiday:\grammar{pl}} \gl{chi}{\grammar{circ}}?’ gofynnodd Hagrid.}
}

\noindent
In \cref{ex:angen} an impersonal \E{you} is reflected by a non-personal construction.

\example{12}{145}{156}{}{angen}
{
	\bi
	{Unfortunately, \hlyou{you} needed a specially signed note from one of the teachers to look in any of the restricted books […]}
	{Yn anffodus, \gl{roedd}{was} \gl{angen}{need} \gl{nodyn}{note} \gl{wedi}{after} \gl{ei}{\grammar{3sg.poss}} \gl{arwyddo}{sign:\grammar{inf}}\gl{’n}{in} \gl{arbennig}{special} gan un o’r athrawon i edrych yn un o’r llyfrau cyfyngedig, […]}
}

\noindent
In \cref{ex:rhywun} an impersonal \E{you} and \E{your} are reflected by \C{rhywun} ‘someone’ and a zero, respectively.

\example{11}{134}{144}{}{rhywun}
{
	\bi
	{‘Blasted thing,’ Snape was saying. ‘How are \hlyou{you} supposed to keep \hlyou{your} eyes on all three heads at once?’}
	{‘Damia fo!’ meddai Sneip. ‘\gl{Sut}{how} \gl{mae}{is} \gl{bosib}{possible} \gl{i}{for} \gl{rywun}{someone} \gl{gadw}{keep:\grammar{inf}} \gl{llygad}{eye} \gl{ar}{on} \gl{y}{the} \gl{tri}{three} \gl{phen}{head} \gl{ar}{on} \gl{unwaith}{once}?’}
}

\noindent
\cref{ex:thankyou,ex:tellyou} demonstrate how some occurrences of \E{you-} in more-or-less bound phrases like \E{tell}\symbolglyph{‿}\E{you} or \E{thank}\symbolglyph{‿}\E{you} do not necessarily reflect in the translation by a person maker.

\example{1}{14}{8}{}{thankyou}
{
	\bi
	{‘No, thank \hlyou{you},’ said Professor McGonagall coldly, […]}
	{‘\gl{Dim}{no (cf.\ Fr.\ \emph{pas})} \gl{diolch}{thanks},’ meddai’r Athro’n oeraidd, […]}
}

\example{4}{41}{38}{}{tellyou}
{
	\bi
	{‘Call me Hagrid,’ he said, ‘everyone does. An’ like I told \hlyou{yeh}, I’m Keeper of Keys at Hogwarts~— yeh’ll know all about Hogwarts, o’ course.’}
	{‘Hagrid ma’ pawb yn ’y ngalw i,’ meddai. ‘Gwna ditha hefyd. \gl{Ac}{and} \gl{fel}{as} \gl{dudish}{speak:\grammar{pst.1sg}} \gl{i}{\grammar{1sg}}, fi ydi Ceidwad Allweddi Hogwarts~— glywaist ti am Hogwarts, wrth gwrs?}
}

A major class of cases in which Welsh has an overt second person marking but English has a zero is the imperatives. Like in any other second person marking in Welsh, the \C{ti:chi} distinction occurs in imperatives as well.

There are two main strategies employed when translating occurrences of an impersonal (or ‘generic’) \E{you-} \parencite[see][]{kitagawa.ch+:1990:impersonal-personal,helmbrecht.j:2015:non-prototypical}.
One is to use an impersonal second person in Welsh as well \parencite[see][§~3.2.4]{flohr.h:2013:potter}.
These uses of second person reflect the sociopragmatic interpersonal relation between the speaker and the addressee through the \C{ti:chi} distinction just like in actual, referential usage.
The other strategy is to rephrase the text so it does not have a person marker but a zero or an overt non-personal element%
\footnote{Usually \C{rhywun} ‘someone’, with one instance of \C{neb} ‘anyone, no one’ and one of \C{pobl} ‘people’.};
\Cref{ex:angen,ex:rhywun} are of this type.



\subsection{Methodology}

The method of gathering the data for analysis is quite straightforward.
The first stage was to write down how each character addresses the other characters in the translation.
Since the vast majority of occurrences of \E{you-}
\begin{table}[htb] % The table is located here in order to avoid a widow. This is an ugly hack.
	\bgroup
	\small
	% an ugly workaround, but it works…
	\begin{tabularx}{\textwidth}{lllXlll}
		\toprule
		speaker         & addressee       & T-V             \\
		\cmidrule(r){1-3}
		\symbolglyph{⋮} & \symbolglyph{⋮} & \symbolglyph{⋮}  &  & Dumbledore      & Harry           & \C{ti}          \\
		Doris Crockford & Harry           & \C{chi}          &  & Dumbledore      & McGonagall      & \C{ti}          \\
		Draco           & Harry           & \C{ti}           &  & Filch           & Harry           & \C{ti}          \\
		Draco           & McGonagall      & \C{chi}          &  & Filch           & Peeves          & \C{ti}          \\
		Draco           & Neville         & \C{ti}           &  & Filch           & Snape           & \C{chi}         \\
		Draco           & Ron             & \C{ti}           &  & Firenze         & Bane            & \C{ti}          \\
		Dudley          & Harry           & \C{ti}           &  & Firenze         & Harry           & \C{chi}         \\
		Dudley          & Vernon          & \C{ti}           &  & Firenze         & Harry           & \C{ti}          \\
		Dumbledore      & Hagrid          & \C{ti}           &  & \symbolglyph{⋮} & \symbolglyph{⋮} & \symbolglyph{⋮} \\
		\bottomrule
	\end{tabularx}
	\egroup
	\caption{%
		A fragment of the table representing\newline
		the address forms used in the corpus
	}
	\label{tab:fragment}
\end{table}
correlate with second person markers in the translation and vice versa, this was done by digitally tagging all occurrences of \E{you-}%
\footnote{1393 occurrences in total~—
	\E{you:} 1034,
	\E{your:} 148,
	\E{yeh:} 121,
	\E{yer:} 60,
	\E{yourself:} 14,
	\E{yours:} 8,
	\E{yourselves:} 5,
	\E{yerself:} 2,
	\E{yerselves:} 1.
	\E{yeh} and \E{yer-} are West Country dialectal forms, used by the character Hagrid.
	The tagged file is available at the above web address as a part of the open research project (see~\cref{fn:english.xml}).%
}
in a file containing the English text with the tuple \textsf{(speaker, addressee, address form)}, where \textsf{address form} reflects the Welsh address form in the translation.
From these tags a long table was derived using an automated script (see \cref{tab:fragment} for a representative fragment).
Then information from imperatives and other instances in which a second person marker in the translation does not correlate with \E{you-} was added.%
\footnote{Such as \C{\gl{os}{if} \gl{gweli}{see:\grammar{prs}.\grammar{2sg}} \gl{di}{\grammar{2sg}}\gl{’n}{in} \gl{dda}{good}} (\C{ti}-form) and \C{os gwelwch chi’n dda} (\C{chi}-form), translating \E{please} (lit.\ ‘if you see well’ or ‘if you see fit’).}
This table represents the intricate map of sociopragmatic relationships in the text, as expressed through the \C{ti:chi} distinction.
