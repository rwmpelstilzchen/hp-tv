children:
ti            | Draco                            | Harry
ti            | Draco                            | Neville
ti            | Draco                            | Ron
ti            | Dudley                           | Harry
ti            | Fred and George                  | Harry
ti            | Fred/George                      | Harry
ti            | Fred/George                      | Percy
ti            | Fred                             | Harry
ti            | Fred                             | Molly
ti            | George                           | Ginny
ti            | George                           | Percy
ti            | George                           | Ron
ti            | Harry                            | Draco
ti            | Harry                            | Dudley
ti            | Harry                            | George
chi-ambiguous | Harry                            | Hermione + Ron / generic
ti            | Harry / Hermione / Ron           | Neville
ti            | Harry                            | Neville
ti            | Harry                            | Ron
ti            | Hermione                         | Harry
ti            | Hermione                         | Ron
ti            | Hermione                         | Neville
ti            | Hermione or Harry                | Ron
ti            | Hermione                         | Percy
ti            | Hermione                         | Ron
ti            | Hermione + Ron                   | Harry
ti            | Hermione / Ron                   | Neville
ti            | Neville                          | Draco
ti            | Neville                          | Harry
ti            | Neville                          | Ron
ti            | one student                      | one student
ti            | Pansy Parkinson                  | Parvati Patil
ti            | Percy                            | Harry
ti            | Piers Polkiss                    | Harry
ti            | Ron                              | Dean
ti            | Ron                              | Draco
ti            | Ron                              | Harry
ti            | Ron                              | Harry / generic
ti            | Ron (/Harry)                     | Hermione
chi-ambiguous | Ron                              | Harry + Hermione / generic
chdi          | Ron                              | Hermione
ti            | Ron                              | Hermione
ti            | Ron                              | Neville
ti            | Slytherin students               | Harry
ti            | some generic/random student      | Harry
ti            | Wood                             | Harry
ti            | Harry                            | Hermione
ti            | Charlie                          | Ron



the rest:
chi-ambiguous | -                                | -
ti            | a chessman                       | Harry
chi-sg        | a goblin at Gringotts            | Hagrid
chi-sg        | a man wearing a violet cloak     | Vernon
chi-sg        | (anonymous)                      | Harry
chi-ambiguous | author                           | audience
chi-ambiguous | author                           | audience / generic
ti            | Bane                             | Firenze
ti            | Bane                             | Hagrid
ti            | (doors at Gringotts)             | (whoever reads)
chi-sg        | Doris Crockford                  | Harry
chi-sg        | Draco                            | McGonagall
ti            | Dumbledore                       | Hagrid
ti            | Dumbledore                       | Harry
ti            | Dumbledore                       | McGonagall
ti            | Filch                            | Harry
ti            | Filch                            | Peeves
chi-sg        | Filch                            | Snape
ti            | Firenze                          | Bane
chi-sg        | Firenze                          | Harry
ti            | Firenze                          | Harry
chi-sg        | Firenze                          | Harry / generic
chi-ambiguous | FIS (Harry)                      | audience / generic
chi-ambiguous | FIS (The Dursleys)               | audience
chi-ambiguous | FIS (Vernon)                     | audience / generic
ti            | Flitwick                         | Hagrid
ti            | Fred/George                      | Molly
chi?          | Gringotts spokesgoblin           | ?
chi-sg        | Hagrid                           | a goblin at Gringotts
ti            | Hagrid                           | Bane
ti            | Hagrid                           | Draco
chi-ambiguous | Hagrid                           | Draco + Harry + Hermione + Neville
chi-sg        | Hagrid                           | Dumbledore
ti            | Hagrid                           | Filch
ti            | Hagrid                           | Griphook
chdi          | Hagrid                           | Harry
ti            | Hagrid                           | Harry
ti            | Hagrid                           | Harry / generic
ti            | Hagrid                           | Hermione
ti            | Hagrid                           | hooded figure
chdi          | Hagrid                           | Neville
ti            | Hagrid                           | Neville
ti            | Hagrid                           | Norbert
ti            | Hagrid                           | Ron
chi-ambiguous | Hagrid                           | Ron?
chdi          | Hagrid                           | Ronan
ti            | Hagrid                           | Ronan
chi-unknown   | Hagrid                           | something in the Forest
ti            | Hagrid                           | Vernon
chi-sg        | Harry                            | Dedalus Diggle
chi-sg        | Harry                            | Dumbledore
chi-sg        | Harry                            | Firenze
chi-sg        | Harry                            | Firenze / generic
chdi          | Harry                            | Hagrid
chi-sg        | Harry                            | Hagrid
ti            | Harry                            | Hagrid
ti            | Harry / Hermione / Neville / Ron | Peeves
chi-sg        | Harry                            | Madam Pomfrey
chi-sg        | Harry                            | McGonagall
chi-sg        | Harry                            | Nearly Headless Nick
chi-sg        | Harry                            | Quirrell
chi-sg        | Harry                            | Snape
ti            | Harry                            | the boa constrictor
chi-sg        | Harry                            | Vernon
ti            | Hermione                         | Hagrid
chi-sg        | Hermione                         | Hagrid / generic
chi-sg        | Hermione                         | McGonagall
ti            | Madam Hooch                      | Neville
ti            | Madam Malkin                     | Harry
chi-sg        | Madam Pince                      | Harry
ti            | Madam Pomfrey                    | Harry
chi-sg        | McGonagall                       | Draco
ti            | McGonagall                       | Dumbledore
ti            | McGonagall                       | Dumbledore / generic
ti            | McGonagall                       | Hagrid
chi-sg        | McGonagall                       | Harry
chi-ambiguous | McGonagall                       | Harry + Hermione + Neville
chi-sg        | McGonagall                       | Hermione
chi-sg        | McGonagall                       | Jordan
chi-sg        | McGonagall                       | Neville
chi-ambiguous | McGonagall                       | Wood
chdi          | Molly                            | Fred
ti            | Molly                            | Fred
ti            | Molly                            | Ginny
ti            | Molly                            | Harry
ti            | Molly                            | Percy
ti            | Molly                            | Ron
ti            | Nearly Headless Nick             | Harry
ti            | Nearly Headless Nick             | Ron
ti            | Ollivander                       | Hagrid
chi-sg        | Ollivander                       | Harry
chi-sg        | Ollivander                       | Harry / generic
ti            | Peeves                           | anyone he would catch
ti            | Peeves                           | Filch
ti            | Peeves → Filch                   | Filch → Peeves
chi-ambiguous | Peeves                           | ? (=Harry + Hermione + Ron)
chi-unknown   | Peeves                           | ? (=Harry + Hermione + Ron)
chi-sg        | Peeves                           | the Bloody Baron (actually, Harry + Hermione + Ron)
ti            | Percy                            | Peeves
ti            | Petunia                          | Dudley
ti            | Petunia                          | Harry
ti            | Petunia                          | Vernon
ti            | Petunia/Vernon                   | Dudley
chi-?         | Quirrell                         | Dumbledore? Everyone in the Hall?
chi-sg        | Quirrell                         | Harry
ti            | Quirrell                         | Snape
chi-sg        | Quirrell                         | Voldemort
ti            | Ronan                            | Hagrid
ti            | Ron                              | Hagrid
chi-sg        | Ron                              | Nearly Headless Nick
chi-ambiguous | Ron                              | the black knight? the chess pieces as a whole?
chi-sg        | Seamus                           | Nearly Headless Nick
chi-sg        | Snape                            | Harry
chi-sg        | Snape                            | Harry / generic
chi-sg?pl?    | Snape                            | Harry? Harry + Hermione + Ron?
ti            | Snape                            | Neville
ti            | Snape                            | Quirrell
chi-sg        | ?                                | the Fat Friar
ti            | the Sorting Hat                  | Harry
ti            | the whole school                 | Hogwarts
chi-unknown   | (title of a book)                | the reader of that book
ti            | Vernon                           | Dudley
chi-sg        | Vernon                           | Hagrid
ti            | Vernon                           | Hagrid
ti            | Vernon                           | Harry
ti            | Vernon                           | Petunia
ti            | Vernon + Petunia                 | Harry
chi-unknown   | Vernon                           | someone unknown at the door (Hagrid)
ti            | Voldemort                        | Harry
chi-unknown   | writer of the note               | ?
