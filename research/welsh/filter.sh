ack "<[a-z][^>]*>" english.xml -o |\
	sed 's/<[a-z]* id="[0-9]*" type="\([^"]*\)" speaker="\([^"]*\)" addressee="\([^"]*\)".*/\1\t\2\t\3/g' |\
	sort | uniq |\
	sed '/^\(-\|<\|am\|\t\|=\|[^ct]\|chi-pl\).*/d' |\
	sort -t$'\t' -k2 -nr
