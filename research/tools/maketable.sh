ack "<y[a-z][^>]*>" ../$1/english.xml -o |\
	sed 's/<\([a-z]*\) id="\([0-9]*\)" type="\([^"]*\)" speaker="\([^"]*\)" addressee="\([^"]*\)".*/\1\t\2\t\3\t\4\t\5/g' |\
	sort --version-sort
